from handlers.index import IndexHandler
from handlers.sensor import SensorHandler

url_patterns = [
    (r'/', IndexHandler),
    (r'/ws', SensorHandler),
]
