#!/usr/bin/env python

import tornado.httpserver
import tornado.ioloop
import tornado.web
from tornado.options import options

from settings import settings
from urls import url_patterns

import logging
logger = logging.getLogger('zwmagicmirror.' + __name__)


class TornadoApp(tornado.web.Application):
    def __init__(self):
        super(TornadoApp, self).__init__(url_patterns, **settings)


def main():
    app = TornadoApp()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port, options.address)

    logger.info("Starting server on %s:%d", options.address, options.port)

    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
