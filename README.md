zwsensors
===============================================================================

## Description

System service providing data from any kind of sensors via WebSocket. It is using
async web framework [Tornado](http://www.tornadoweb.org/) and is designed to provide
easy access to [Raspberry](http://www.raspberrypi.org/) Pi PINs for Internet of Things.


## Requirements

* Raspberry Pi
* [Raspbian](http://www.raspbian.org/)
* Python 2.7
* virtualenvwrapper
* supervisord


## Supported sensors

For now we just support only this sensors:

* DHT11 (temperature and humidity sensor)
* DHT22 (temperature and humidity sensor)


## Installation

1. Create folder for installation
1. Clone current version of code
1. Copy files from deploy folder to supervisiord config path
1. Reset system and use!


## Usage

TBD


## Future plans

* Add new sensors (waiting ones are: BMP085 and BH1750FVI)
* Add support for remote sensors
* Add setup process with saving everything into database 


## Contributing

If you have improvements or bug fixes:

* Fork the repository on bitbucket
* File an issue for the bug fix/feature request in bitbucket
* Create a topic branch
* Push your modifications to that branch
* Send a pull request

## Authors

* Adam Strojek
