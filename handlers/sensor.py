import time
import threading
import tornado.websocket

import Adafruit_DHT

import logging
logger = logging.getLogger('zwmagicmirror.' + __name__)


class SensorHandler(tornado.websocket.WebSocketHandler):
    client_list = []

    def check_origin(self, origin):
        logger.debug("Check origin: {0}".format(origin))
        return True

    def open(self):
        logger.debug("New client connected")
        if self not in self.client_list:
            self.client_list.append(self)

    def on_close(self):
        logger.debug("Client disconnected")
        if self in self.client_list:
            self.client_list.remove(self)


def bin2dec(string_num):
    return str(int(string_num, 2))


class SensorThread(threading.Thread):
    def __init__(self, model, pin):
        super(SensorThread, self).__init__()
        self.model = model
        self.pin = pin

    def send_message(self, temperature, humidity):
        if SensorHandler.client_list:
            logger.debug("Sending a message %sC, %s%%", temperature, humidity)
            payload = {
                'temperature': temperature,
                'humidity': humidity,
            }
            for client in SensorHandler.client_list:
                client.write_message(payload)
        else:
            logger.debug("Nobody to send")

    def run(self):
        while True:
            humidity, temperature = Adafruit_DHT.read_retry(self.model, self.pin)
            if humidity is not None and temperature is not None:
                self.send_message(temperature, humidity)
                # But if everything finished with success wait 1 minute
                time.sleep(60)
            else:
                # In other case try to get new info in short delay
                time.sleep(1)

sensor_thread = SensorThread(Adafruit_DHT.DHT22, 4)
sensor_thread.start()